package com.example.mrhead2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    ImageView mata;
    ImageView kumis;
    ImageView alis;
    ImageView rambut;
    ImageView jenggot;
    ImageView badan;
    CheckBox btnRambut;
    CheckBox btnKumis;
    CheckBox btnAlis;
    CheckBox btnJenggot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        badan = findViewById(R.id.badan);
        jenggot = findViewById(R.id.jenggot);
        rambut = findViewById(R.id.rambut);
        alis = findViewById(R.id.alis);
        kumis = findViewById(R.id.kumis);
        mata = findViewById(R.id.mata);
        btnRambut = findViewById(R.id.btnRambut);
        btnJenggot = findViewById(R.id.btnJenggot);
        btnKumis = findViewById(R.id.btnKumis);
        btnAlis = findViewById(R.id.btnAlis);

        btnRambut.setOnClickListener(this);
        btnJenggot.setOnClickListener(this);
        btnAlis.setOnClickListener(this);
        btnKumis.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        if(btnAlis.getId()==view.getId()){
            if (alis.getVisibility()==View.INVISIBLE){
                alis.setVisibility(View.VISIBLE);
            }
            else {
                alis.setVisibility(View.INVISIBLE);
            }}
        if(btnKumis.getId()==view.getId()){
            if(kumis.getVisibility()==View.INVISIBLE){
                kumis.setVisibility(View.VISIBLE);
            }
            else{kumis.setVisibility(View.INVISIBLE);}
        }
        if(btnJenggot.getId()==view.getId()){
            if(jenggot.getVisibility()==View.INVISIBLE){
                jenggot.setVisibility(View.VISIBLE);
            }
            else{jenggot.setVisibility(View.INVISIBLE);}
        }
        if(btnRambut.getId()==view.getId()){
            if(rambut.getVisibility()==View.INVISIBLE){
                rambut.setVisibility(View.VISIBLE);
            }
            else{rambut.setVisibility(View.INVISIBLE);}
        }


    }}
